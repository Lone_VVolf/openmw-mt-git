# Maintainer: Lone_Wolf <lone_wolf@klaas-de-kat.nl>
# Contributor: Sandy Carter <bwrsandman@gmail.com>
# Contributor: Sven-Hendrik Haase <svenstaro@gmail.com>

pkgname=openmw-mt-git
_name=openmw

pkgver=0.48.0.r27915.dd4f1e3044
_pkgver=0.48.0
pkgrel=1
pkgdesc="Open-source engine reimplementation for the role-playing game Morrowind, built with multithreaded physics support"
arch=('x86_64')
url="http://www.openmw.org"
license=('GPL3' 'MIT' 'custom')
makedepends=('cmake' 'boost' 'doxygen' 'git')
depends=('openal' 'openscenegraph-openmw-git' 'mygui' 'bullet-multithreaded' 'qt5-base' 'ffmpeg4.4' 'sdl2' 'unshield' 'libxt' 'boost-libs' 'luajit' 'sqlite' 'recastnavigation-openmw' 'icu' 'yaml-cpp')
provides=('openmw')
conflicts=('openmw')
source=('git+https://gitlab.com/OpenMW/openmw.git'
                'donot-add-new-content-list.patch'
)
sha512sums=('SKIP'
            '31959e59f2fa9c8f9be9fb9df5eae2c8c9810262fb08e5db7bc6ddc5b43709ee91040f1a645c70578a16b9758f1e41bb6413c20ea9b56a01da65e281a6955c38'
)
 options=(debug strip)


pkgver() {
    cd $_name
    printf "%s.r%s.%s" "${_pkgver}" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
    # an adjustment of the example in wiki usable when upstream doesn't use tags (or neglects to maintain them)
}

prepare() {
    patch --directory=${_name} --strip=1 --input="${srcdir}/donot-add-new-content-list.patch"
 }

build() {
    export PKG_CONFIG_LIBDIR=/usr/lib/ffmpeg4.4/pkgconfig/
    cmake \
        -B _build \
        -S "${srcdir}/${_name}"  \
        -D CMAKE_INSTALL_PREFIX=/usr \
        -D LICDIR=/usr/share/licenses/${pkgname} \
        -D CMAKE_BUILD_TYPE=None \
        -D OPENMW_LTO_BUILD=ON \
        -D OPENMW_USE_SYSTEM_RECASTNAVIGATION=ON 

        make  -C _build
}

package() {
    make DESTDIR="${pkgdir}" -C _build install
    install -Dm 644 "${_name}/LICENSE" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
